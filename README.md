# DealerSys

DealerSys is an application that keeps track of a car dealership inventory, sales and services. The application allows for ease of use from adding inventory of vehicles, salespeople, customers and technicians. The app also allows for creation and tracking of sales and service appointments. All of these features are here to help your dealership manage important data today!

Team:

* Erick Watanabe - Service
* Jonathan Alexander - Sales

## How to Run this App
**Requirements**
    -Docker-compose (The application depends on building out the services into separate containers)
    -Node JS
    -Git

**How to install and use**
1. Fork and clone the DealerSys repository located at the top of this page
2. Create a directory on your local machine that you would like the application located in.
3. Be in the directory you want the application in and use the command:
    ```
     git clone <Link to your clone of the application here>
     ```
4. Use the following commands in the order specified to start up Docker:
    ```
    docker volume create beta-data
    docker-compose build
    docker-compose up
    ```
5. Building and running the application can take some time. Wait for all your docker containers to be running before trying to use the app. Maybe grab some coffee, or pizza?

6. In your browser, navigate to http://localhost:3000/

7. Warning, certian features with in the appointment models will not work as intended until there is at least one automobile in inventory. It is suggested to:
    ```
    navagate to "Create Manufacturer" and create a manufacture
    navagate to "Create Model" and create a vehical model
    navagate to "Add Automobile to Inventory" and add an Automobile to Inventory
    ```


## Diagram
 A diagram showing the 3 microservices and their interactions:
    -Inventory
    -Sales
    -Services

![Img](/images/CarCarDiagramIMG.png)

## API Documentation

## Integration

Our Inventory, Sales, and Service domains work in conjunction here at our DealerSys Company.

Our Inventory microservice can keep track of all the cars added and sold from our lot based on their VIN. All the VIN’s are stored in the Inventory. Both our Service and Sales microservices work with the Inventory domain by using a poller. The poller uses the AutomobileVO model to exchange all automobile VIN’s with Service and Sales. It exchanges information every 60 seconds to keep Sales and Services up to date with the cars that have been sold from the lot, and if customers will get VIP treatment if they want to get their car serviced here at DealerSys.

# Inventory

## Access Endpoints to Send and View Data. This can be done through API testing tool like insomnia or your browser

It is recommended to go in the following order to get the most out of our application. Manufacturer -> Modles -> Automobiles

## Manufacturers:
The <int:pk> is the "id" in the object that is returned, it is a number.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/<int:pk>/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/<int:pk>/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/<int:pk>/

Use JSON body to send data:

To create a Manufacture you can use the following. Note you can only make a single manufacture at a time
```
{
  "name": "Honda"
}
```

This will return data that looks something like:
```
{
	"href": "/api/manufacturers/4/",
	"id": 4,
	"name": "Honda"
}
```

Getting a list of manufacturers will return a dictionary with a key of "manufactures", which will have an array of each manufacture as its dictionary like the one returned above. It could look something like this:
```
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Audi"
		},
		{
			"href": "/api/manufacturers/4/",
			"id": 4,
			"name": "Honda"
		}
	]
}
```

## Vehicle Models:

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/<int:pk>/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/<int:pk>/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/<int:pk>/

To create a vehicle Model you can use the following. Note you can only make a single model at a time
```
{
  "name": "civic",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/6/6d/2017_Honda_Civic_SR_VTEC_1.0_Front.jpg",
  "manufacturer_id": 4
}
```

This will return data that looks something like:
```
{
	"href": "/api/models/2/",
	"id": 2,
	"name": "civic",
	"picture_url": "https://upload.wikimedia.org/wikipedia/commons/6/6d/2017_Honda_Civic_SR_VTEC_1.0_Front.jpg",
	"manufacturer": {
		"href": "/api/manufacturers/4/",
		"id": 4,
		"name": "Honda"
	}
}
```

Getting a list of models will return a dictionary with a key of "manufactures", which will have an array of each model as its dictionary like the one returned above. It could look something like this:
```
{
	"models": [
		{
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Audi"
		},
		...
	]
}
```

## Automobiles:
The **VIN** is how each car is identified, its unique to each car. The **VIN** in the following endpoint are all expected to be strings. All letters and numbers will be converted type string on the backend
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/

To get a list of all the automobiles in our invetnory run a GET request, it will return a dictionay with the key "autos" that is a list of all the cars. What is returned could look like:
```
{
	"autos": [
		{
			"href": "/api/automobiles/1E3BB5FB2AN120174/",
			"id": 1,
			"color": "red",
			"year": 2012,
			"vin": "1E3BB5FB2AN120174",
			"model": {
				"href": "/api/models/1/",
				"id": 1,
				"name": "Sebring",
				"picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
				"manufacturer": {
					"href": "/api/manufacturers/2/",
					"id": 2,
					"name": "Nissan"
				}
			},
			"sold": true
		},
	]
}
```

To create an automobile you send something like the following JSON body:
```
{
  "color": "blue",
  "year": 2023,
  "vin": "1A1AAFB2AN120174",
  "model_id": 2
}
```
This will return:
```
{
	"href": "/api/automobiles/1A1AAFB2AN120174/",
	"id": 2,
	"color": "blue",
	"year": 2023,
	"vin": "1A1AAFB2AN120174",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "A4",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/6/6d/2017_Honda_Civic_SR_VTEC_1.0_Front.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	},
	"sold": false
}
```
To get the details of a specific automobile, you can query by its VIN:
example url:
```
http://localhost:8100/api/automobiles/1A1AAFB2AN120174/
```
The return value:
```
{
	"href": "/api/automobiles/1A1AAFB2AN120174/",
	"id": 2,
	"color": "blue",
	"year": 2023,
	"vin": "1A1AAFB2AN120174",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "A4",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/6/6d/2017_Honda_Civic_SR_VTEC_1.0_Front.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	},
	"sold": false
}
```

You can update the color, year, or sold properties, with a PUT request and VIN of the car you want to update, the JSON body can look something like:
```
{
  "color": "red",
  "year": 2020,
  "sold": true
}
```
The return value for the update will look something like:
```
{
	"href": "/api/automobiles/1A1AAFB2AN120174/",
	"id": 2,
	"color": "red",
	"year": 2020,
	"vin": "1A1AAFB2AN120174",
	"model": {
		"href": "/api/models/2/",
		"id": 2,
		"name": "A4",
		"picture_url": "https://upload.wikimedia.org/wikipedia/commons/6/6d/2017_Honda_Civic_SR_VTEC_1.0_Front.jpg",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Audi"
		}
	},
	"sold": true
}
```


### URLs and Ports

**MAIN APPLICATION URL**

http://localhost:3000/

**Sales microservice Frontend URL'S**

| URL | Description |
| --- | --- |
| http://localhost:3000/create_salesperson | A form that allows the client to create a new salesperson and requires the person name and employee ID. |
| http://localhost:3000/listSalespeople | Displays the list of all current salespeople by name, id |
| http://localhost:3000/createCustomer | Takes the client to a creation form used to store customer information(name, address, phone number). |
| http://localhost:3000/listCustomers | Takes the client to the current list of all customers stored in the database. |
| http://localhost:3000/salesform | Takes the client to the sales form and requires vehicle VIN #, saleperson name, customer name, and cost of the vehicle. Once submitted, the sale is created and listed in the saleslist and salesperson history. |
| http://localhost:3000/saleslist | Displays the list of currently stored sales. The list contains the salesperson employee ID, salesperson name, customer name, vehicle VIN(sold) and sale price. |

**Sales microservice Backend URL'S**

| URL | Description |
| --- | --- |
| http://localhost:8090/api/salespeople/ | List of all currently stored salespeople(GET request) or creation of new saleperson(POST request). |
| http://localhost:8090/api/customers/ | List of all currently stored customers(GET request) or creation of customer(POST request). |
| http://localhost:8090/api/sales/ | List of all currently stored sales(GET request) or creation of new sale via the sales form(POST request) |


## Service API
Service keeps track of our technicians and service appointments. If cars were purchased from our lot that customer will be marked as VIP by the service microservice.

Its recommemned to have at least one automobile in inventory. See Inventory documentation above to create a automobile.

Its also recommended to have at least on technician before using appointment services. You can add technition in the steps below:
### Technician
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create a technician | POST | http://localhost:8080/api/technicians/
| List technicians | GET | http://localhost:8080/api/technicians/
| Delete a technician | DELETE | http://localhost:8080/api/technicians/<int:id>/

#### Add a technician
To create a technician use the POST method above. You can only add a single technician at a time. You must provide the first and last name and the employee id the JSON body should look something like:
```
{
	"first_name": "Erick",
	"last_name": "Watanabe",
	"employee_id": "1"
}
```
The "id" at the top is automatically generated and will be the way our system keeps track of each technician. It will return something like:
```
{
	"id": 6,
	"first_name": "Erick",
	"last_name": "Watanabe",
	"employee_id": "1"
}
```

#### GET a list of all technicians
Use the GET method above and provide nothing for the body. It will return something like:
```
{
	"technicians": [
		{
			"id": 3,
			"first_name": "Noodle",
			"last_name": "Djen-Watanabe",
			"employee_id": 1
		},
		{
			"id": 6,
			"first_name": "Erick",
			"last_name": "Watanabe",
			"employee_id": 1
		}
	]
}
```

#### Delete an existing technician
To delete you must place the id of the technician you want to delete in the url for DELETE. If succesful, the return will look like:
```
{
	"is_deleted": true
}
```

### Appointments
| Action | Method | URL
| ----------- | ----------- | ----------- |
| Create service appointment | POST | http://localhost:8080/api/appointments/
| List service appointments | GET | http://localhost:8080/api/appointments/
| Delete service appointment | DELETE | http://localhost:8080/api/appointments/<int:id>/
| Set appointment status to cancel | PUT | http://localhost:8080/api/appointments/<int:id>/cancel/
| Set appointment status to  finish | PUT | http://localhost:8080/api/appointments/<int:vin>/finish/

#### Creating a service appointment
Use the POST request method above. The JSON body must include the vin, customer name, date, time, technicianm and reason for the appointment. The JSON body could look something like:
```
{
	"vin": "1G1OPHS2120174",
	"customer": "Claire Watanabe",
	"date": "2024-01-22",
	"time": "04:00",
	"technician": 1,
	"reason": "The technicians are all dogs, I just wanted to come"
}
```
This will return an object. The date and time will be combined to a dateTime python object, and the first and last name, and technicians employee id will be added to the object that is returned. The "id" at the top is automatically generated and will be the way our system keeps track of each appointment. The return object could look something like:
```
{
	"id": 9,
	"date_time": "2024-01-22T04:00:00",
	"reason": "The technicians are all dogs, I just wanted to come",
	"status": "created",
	"vin": "1G1OPHS2120174",
	"customer": "Claire Watanabe",
	"technician": {
		"id": 1,
		"first_name": "Gopher",
		"last_name": "Signabe",
		"employee_id": 3
	}
}
```

#### GET a list of appointments
Use the GET for "List service appointments". No JSON body should be used. It will return an object with "appointments" as the key with an array of appointment objects. A list of appointments could look like:
```
{
	"appointments": [
		{
			"id": 4,
			"date_time": "2023-06-10T10:44:00+00:00",
			"reason": "makes a weird nooise",
			"status": "created",
			"vin": "1OLDCHIRPY20174",
			"customer": "Momma Djen",
			"technician": {
				"id": 3,
				"first_name": "Noodle",
				"last_name": "Djen-Watanabe",
				"employee_id": 1
			}
		},
        ...
	]
}
```

#### To DELETE an appointment
Use the DELETE method. You must place "id" number for the appointment you want to delete in the url. When successful it will return object like:
```
{
	"is_deleted": true
}
```

#### To change the status of an appointment
An appointment can have three statuses: created, canceled, or finished. When an appointment is created its status is set to "created". This can be changed by using the two PUT requests. You need the id of the appointment you want to change the status of place either, cancel or finsh, at the end of the url, both corresponding to canceling or finishing an appointment, respectfully. When submitted it will return the oppointment object with the status changed, depending on which url is used. An example of an appointment that has been cancel looks like:
```
{
	"id": 2,
	"date_time": "2023-06-10T10:33:00+00:00",
	"reason": "WHY VIP, WHJY DO YOY DO THIS?",
	"status": "canceled",
	"vin": "4Y1SL65848Z411439",
	"customer": "Erick",
	"technician": {
		"id": 1,
		"first_name": "Gopher",
		"last_name": "Signabe",
		"employee_id": 3
	}
}
```


### Sales API

The Sales API consists of the capablility to create/list(read)/delete customers, create/list(read)/delete salespeople and create/list(read)/delete Sales records. These requests are made via GET, POST and DELETE.

The models in the Sale-API consists of AutomobileVO, Customers, Saleperson and Sales. The poller will update the Automobile information to the sales AutomobileVO to keep track of any changes. The Sales-API will need to have access to the automobile to determine if the vehicle has been sold or not. In the event of a sale, the automobile sold variable is changed so that the vehicles availability can reflect the change.

## Sales

| Request | URL | Description |
| --- | --- | --- |
| GET | http://localhost:8090/api/sales/  | Gets the list of all currently stored sales. |
| POST | http://localhost:8090/api/sales/ | Creates a new sale. |
| DELETE | http://localhost:8090/api/sales/:id/ | Deletes a sale at the specified id number within the link. |

Example Sales GET return value:
```
{
	"sales": [
		{
			"id": 1,
			"automobile": {
				"vin": "1C3CC5FB2AN120171",
				"sold": false
			},
			"salesperson": {
				"id": 1,
				"first_name": "Brendan",
				"last_name": "Eich",
				"employee_id": 1995
			},
			"customer": {
				"id": 1,
				"first_name": "Roger",
				"last_name": "Rabbit",
				"address": "1234 Looney Street, Toontown, TT 12345",
				"phone_number": "555-555-5555"
			},
			"price": 50000.0
		}
	]
}
```

Example Sales POST data **The VIN must be available in automobile list for the sale to post**:

```
{
	"automobile": "1C3CC5FB2AN120171",
	"salesperson": 1,
	"customer": 1,
	"price": 50000.00
}
```
The return list after the POST:

```
{
	"sales": [
		{
			"id": 1,
			"automobile": {
				"vin": "1C3CC5FB2AN120171",
				"sold": false
			},
			"salesperson": {
				"id": 1,
				"first_name": "Brendan",
				"last_name": "Eich",
				"employee_id": 1995
			},
			"customer": {
				"id": 1,
				"first_name": "Roger",
				"last_name": "Rabbit",
				"address": "1234 Looney Street, Toontown, TT 12345",
				"phone_number": "555-555-5555"
			},
			"price": 50000.0
		}
	]
}
```



## Customers

| Request | URL | Description |
| --- | --- | --- |
| GET | http://localhost:8090/api/customers/  | Gets the list of all currently stored customers. |
| POST | http://localhost:8090/api/customers/ | Creates a new customer. |
| DELETE | http://localhost:8090/api/customers/:id/ | Deletes a customer at the specified id number within the link. |

Example Customers GET return value:
```
{
	"customers": [
		{
			"id": 1,
			"first_name": "Roger",
			"last_name": "Rabbit",
			"address": "1234 Looney Street, Toontown, TT 12345",
			"phone_number": "555-555-5555"
		}
	]
}
```

Example Customers POST data:

```
{
	"first_name": "Jessica",
	"last_name": "Rabbit",
	"address": "1234 Looney Street, Toontown, TT 12345",
	"phone_number": "555-555-5554"
}
```
The return list after the POST:

```
{
	"customers": [
		{
			"id": 1,
			"first_name": "Roger",
			"last_name": "Rabbit",
			"address": "1234 Looney Street, Toontown, TT 12345",
			"phone_number": "555-555-5555"
		},
		{
			"id": 2,
			"first_name": "Jessica",
			"last_name": "Rabbit",
			"address": "1234 Looney Street, Toontown, TT 12345",
			"phone_number": "555-555-5554"
		}
	]
}
```


## Salespeople

| Request | URL | Description |
| --- | --- | --- |
| GET | http://localhost:8090/api/salespeople/  | Gets the list of all currently stored salespeople. |
| POST | http://localhost:8090/api/salespeople/ | Creates a new salesperson. |
| DELETE | http://localhost:8090/api/salespeople/:id/ | Deletes a salesperson at the specified id number within the link. |

Example Salepeople GET return value:
```
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Brendan",
			"last_name": "Eich",
			"employee_id": 1995
		}
	]
}
```

Example Salepeople POST data:

```
{
	"first_name": "Sam",
	"last_name": "Taggart",
	"employee_id": "1234"
}
```
The return list after the POST:
```
{
	"salespeople": [
		{
			"id": 1,
			"first_name": "Brendan",
			"last_name": "Eich",
			"employee_id": 1995
		},
		{
			"id": 2,
			"first_name": "Sam",
			"last_name": "Taggart",
			"employee_id": 1234
		}
	]
}
```



## Value Objects

 ## Sales Value Objects:

AutomobileVO - Maintains the VIN data and Sold status that is used in the sales API. When a vehicle is sold, the vehicle will no longer be listed in the sales form as a selection. The sold status of a vehicle is also changed when a sale form is submitted to removed that vehicle from the listed selections.

## Service Value Objects:

AutomobileVO keeps track of the VIN and sold status that is stored in inventory. The VIN is used on the front end to determine if the customer is a VIP customer, meaning that the person bought the car of our lot. The front end loops through all the list of automobiles in inventory and compares it with the VIN associated with the appointment being made.
