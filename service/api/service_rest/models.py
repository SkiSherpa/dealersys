from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField()

    def __str__(self):
        return f"{self.first_name} {self.last_name} - {self.employee_id}"

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.vin}, is sold: {self.sold}"

class Appointment(models.Model):
    date_time = models.DateTimeField(auto_now=False)
    reason = models.TextField()
    status = models.CharField(max_length=100)
    vin = models.CharField(max_length=100)
    vip = models.BooleanField(default=False)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.PROTECT
    )

    def __str__(self):
        return f"vin: {self.vin} | customer: {self.customer}"
