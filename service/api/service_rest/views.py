from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import Technician, AutomobileVO, Appointment
from datetime import datetime


class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin",
        "sold"
    ]

class AppointmentEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "vip"
    ]
    encoders = {
        "technician": TechnicianEncoder(),
    }

@require_http_methods(["POST", "GET"])
def api_list_technicians(request):
    if request.method == "POST":
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianEncoder,
            safe=False,
        )
    else:
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianEncoder
        )

@require_http_methods(["DELETE"])
def api_delete_technician(request, id):
    if request.method == "DELETE":
        try:
            technician = Technician.objects.filter(id=id)
            deleted_technician = technician.delete()

            response = JsonResponse(
                {"is_deleted": deleted_technician[0] > 0}
            )
            return response
        except technician.DoesNotExist:
            response = JsonResponse(
                {"message": "technician does not exist"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=content["technician"])
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician doesn't exist at that id"},
                status=400,
            )
        date_str = content["date"]
        time_str = content["time"]
        datetime_str = f"{date_str} {time_str}"
        datetime_obj = datetime.strptime(datetime_str, "%Y-%m-%d %H:%M")
        content["date_time"] = datetime_obj
        del content["date"]
        del content["time"]
        content["status"] = "created"
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentEncoder,
            safe=False
        )

@require_http_methods(["DELETE"])
def api_delete_appointment(request, id):
    if request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=id)
            deleted_appointment = appointment.delete()
            response = JsonResponse(
                {"is_deleted": deleted_appointment[0] > 0}
            )
            return response
        except appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "appointment does not exist"}
            )
            response.status_code = 400
            return response

@require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "canceled"
        appointment.save()
    except appointment.DoesNotExist:
        response = JsonResponse(
                {"message": "appointment does not exist"}
        )
        response.status_code = 400
        return response
    changed_appointment_status = appointment
    return JsonResponse(
        changed_appointment_status,
        encoder=AppointmentEncoder,
        safe=False,
    )

@require_http_methods(["PUT"])
def api_finish_appointment(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = "finished"
        appointment.save()
    except appointment.DoesNotExist:
        response = JsonResponse(
                {"message": "appointment does not exist"}
        )
        response.status_code = 400
        return response
    changed_appointment_status = appointment
    return JsonResponse(
        changed_appointment_status,
        encoder=AppointmentEncoder,
        safe=False,
    )
