import React, { useState } from 'react';

function SalesForm(props) {
  const [automobile, setAuto] = useState('');
  const [salesperson, setSalesperson] = useState('');
  const [customer, setCustomer] = useState('');
  const [price, setPrice] = useState('');

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      automobile,
      salesperson,
      customer,
      price,
    };

    const locationURL = 'http://localhost:8090/api/sales/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(locationURL, fetchConfig);

    if (response.ok) {
      const newCustomer = await response.json();
      console.log(newCustomer);


        const updatedData = {
          color: automobile.color,
          year: automobile.year,
          sold: true
        };
        const autoURL = `http://localhost:8100/api/automobiles/${automobile}/`;
        const updatedFetchConfig = {
          method: "put",
          body: JSON.stringify(updatedData),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const updateSoldResponse = await fetch(autoURL, updatedFetchConfig);

        console.log(updateSoldResponse)

        setAuto('');
        setSalesperson('');
        setCustomer('');
        setPrice('');
        props.getSales();
    }
  }

    function handleAutomobileChange(event){
        const value = event.target.value;
        setAuto(value)
    }

    function handleSalespersonChange(event){
        const value = event.target.value;
        setSalesperson(value)
    }

    function handleCustomerChange(event){
        const value = event.target.value;
        setCustomer(value)
    }

    function handlePriceChange(event){
        const value = event.target.value;
        setPrice(value)
    }

    const filteredAutos = props.automobiles ? props.automobiles.filter(
      (auto) => auto.sold === false
    )
  : [];


    return(
        <div>
        <h1>Record a new sale</h1>
        <form onSubmit={handleSubmit} id="create-vehicle-form">
            <div className="mb-3">
            <label htmlFor="automobile">Automobile VIN</label>
              <select onChange={handleAutomobileChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                <option value="">Choose an automobile VIN...</option>
                {filteredAutos.map(automobile => {
                  return (
                    <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
            <label htmlFor="salesperson">Salesperson</label>
              <select onChange={handleSalespersonChange} value={salesperson} required name="salesperson" id="automobile" className="form-select">
                <option value="">Choose a salesperson...</option>
                {props.salespeople.map(salesperson => {
                  return (
                    <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
            <label htmlFor="automobile">Customer</label>
              <select onChange={handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                <option value="">Choose a customer...</option>
                {props.customers.map(customer => {
                  return (
                    <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                  )
                })}
              </select>
            </div>
            <div className="mb-3">
              <label htmlFor="price">Price</label>
              <textarea onChange={handlePriceChange} value={price} placeholder="0" required type="number" name="price" id="price" className="form-control" />
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
    </div>
    )
}
export default SalesForm
