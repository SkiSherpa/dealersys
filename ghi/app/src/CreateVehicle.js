import React, { useState } from "react";

function CreateVehicle(props){
    const [name, setName] = useState('');
    const [picture_url, setPicture] = useState('')
    const [manufacturer, setManufacturer] = useState('')


      async function handleSubmit(event) {
        event.preventDefault();
        const data = {
          name,
          picture_url,
          manufacturer_id: manufacturer,
        };


        const locationUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {

          setName('');
          setPicture('');
          setManufacturer('');
          props.getAutoModels();
        }
      }


function handleNameChange(event) {
    const value = event.target.value;
    setName(value);
}

function handlePictureChange(event) {
    const value = event.target.value;
    setPicture(value);
}

function handleManufacturerChange(event) {
    const value = event.target.value;
    setManufacturer(value);
}


return(
    <div>
        <h1>Create a Vehicle Model</h1>
        <form onSubmit={handleSubmit} id="create-vehicle-form">
            <div className="form-floating mb-3">
              <input onChange={handleNameChange} value={name} placeholder="Model Name" required type="text" name="name" id="name" className="form-control" />
              <label htmlFor="name">Model Name</label>
            </div>
            <div className="mb-3">
              <label htmlFor="picture">Picture URL</label>
              <textarea onChange={handlePictureChange} value={picture_url} placeholder="Picture URL" required type="text" name="picture" id="picture" className="form-control" />
            </div>
            <div className="mb-3">
              <select onChange={handleManufacturerChange} value={manufacturer} required name="manufacturer" id="manufacturer" className="form-select">
                <option value="">Choose a Manufacturer</option>
                {props.manufacturerList.map(manufacturer => {
                  return (
                    <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
    </div>
)

}
export default CreateVehicle
