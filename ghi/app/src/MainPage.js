// import landingPic from "../public/landingPic.png";

function MainPage() {
	return (
		<div className="px-4 py-5 my-5 text-center">
			<h1 className="display-5 fw-bold">DealerSys</h1>
			<div className="col-lg-6 mx-auto">
				<p className="lead mb-4">
					The premiere solution for automobile dealership management!
				</p>
			</div>
			<img
				src="/landingPic.png"
				alt="A picture of a car"
				style={{
					width: "800px",
					height: "auto",
					borderRadius: "15px",
					boxShadow: "0 4px 8px rgba(0, 0, 0, 0.2)",
				}}
			/>
		</div>
	);
}

export default MainPage;
