import {useState, useEffect} from "react"

function ServiceHistory(props) {

  const [searchInput, setSearchInput] = useState("");
  console.log(props.appointments);
  const [currentAppointments, setCurrentAppointments] = useState([]);


  function IsVip(vin) {
    const matchingVin = props.automobiles.find(auto => auto.vin === vin);
    return matchingVin ? "YES" : "NO";
  }

  function handleChange(e) {
    e.preventDefault();
    const value = e.target.value;
    setSearchInput(value);
    console.log(currentAppointments, searchInput);
  }

  function handleClick(searchVin) {
    const filteredAppointments = props.appointments.filter(
      (appointment) => appointment.vin === searchVin
      );
      setCurrentAppointments(filteredAppointments);
    }

  useEffect(() => {
    setCurrentAppointments(props.appointments);
  }, [props.appointments]);

    return (
    <div>
      <h1>Service History</h1>

      <div className="input-group mb-3">
        <input
          onChange={handleChange}
          type="search"
          className="form-control"
          placeholder="Search VIN"
          aria-label="Search VIN"
          aria-describedby="basic-addon2"
        />
        <div className="input-group-append">
          <button
            className="input-group-text"
            id="basic-addon2"
            onClick={() => {
              handleClick(searchInput);
            }}
          >
            Search
          </button>
        </div>
      </div>

      <table className="table table-striped">
          <thead>
            <tr>
              <th>Vin</th>
              <th>Customer Name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Technician</th>
              <th>Reason</th>
              <th>VIP</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {currentAppointments.map((appointment) => {
              const date = appointment.date_time.slice(0, 10);
              const time = appointment.date_time.slice(11, 16);
              const isVip = IsVip(appointment.vin);
              return (
                <tr key={appointment.id}>
                  <td>{appointment.vin}</td>
                  <td>{appointment.customer}</td>
                  <td>{date}</td>
                  <td>{time}</td>
                  <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                  <td>{appointment.reason}</td>
                  <td>{isVip}</td>
                  <td>{appointment.status}</td>
                </tr>
              );
            })}
          </tbody>
        </table>

    </div>
  );
}

export default ServiceHistory;
